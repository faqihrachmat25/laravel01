@extends('layout.master')

@section('judul')
	<h1>Media Online</h1>
@endsection
	
@section('content')
	<h3>Sosial media developer</h3>
	<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

	<h3>Benefit Join di Media Sosial</h3>
	<ul>
		<li>Mendapatkan motivasi dari sesama para Developer</li>
		<li>Sharing knowledge</li>
		<li>Dibuat oleh calon web developer terbaik</li>
	</ul>

	<h3>Cara bergabung ke Media Online</h3>
	<ol>
		<li>Mengunjungi website ini</li>
		<li>Mendaftarkan di <a href="signup">Form Sign up</a></li>
		<li>Selesais</li>
	</ol>
@endsection
	
	
