@extends('layout.master')

@section('judul')
	<h1>Welcome</h1>
@endsection

@section('content')
	<!-- Ucapan Selamat  -->
	<h1>SELAMAT DATANG {{$fname}} {{$lname}}</h1>
	<h2>Termakasih telah beragabung di website kami , Media Belajar kita bersama !</h2>
	
	<br>
	<br>
    <p>Berikut deskripsi singkat tentang dirimu : </p>
	<br>
     <p>Nama :{{$fname}}{{$lname}}</p><br>
    <p>Jenis Kelamin :{{$gender}}</p><br>
    <p>Kenegaraan :{{$nation}}</p><br>
    <p>Bahasa : {{$lang}}</p><br>
    <p>Bio :{{$bio}}</p><br> 
	<a href="/">Home</a>
@endsection
