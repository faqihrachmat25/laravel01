@extends('layout.master')

@section('judul')
	<!-- Judul -->
	<h1>Buat Account Baru</h1>
@endsection
	
@section('content')
	<!-- Form -->
	<h2>Sign Up Form</h2>
	<form action="/send" method="post">
        @csrf
		<label for="firstname">First Name :</label><br><br>
		<input type="text" name="firstname" id="firstname" required=""><br><br>
		<label for="lastname">Last Name :</label><br><br>
		<input type="text" name="lastname" id="lastname" required=""><br><br>

		<label>Gender :</label><br><br>
		<input type="radio" name="gender" value="male" required="">Male<br>
		<input type="radio" name="gender" value="female" required="">Female<br>
		<input type="radio" name="gender" value="other" required="">Other<br><br>

		<label>Nationality :</label><br><br>
		<select name="nation">
			<option value="Indonesia">Indonesia</option>
			<option value="America">America</option>
			<option value="England">England</option>
		</select><br><br>

		<label>Language Spoken :</label><br><br>
		<input type="checkbox" id="lang1" name="lang" value="indonesia" >Bahasa Indonesia<br>
		<input type="checkbox" id="lang2" name="lang" value="english" >English<br>
		<input type="checkbox" id="lang3" name="lang" value="other" >Other<br><br>

		<label for="bio">Bio</label><br><br>
		<textarea name="bio" id="bio" cols="30" rows="10" required=""></textarea><br> 

		<input type="submit" value="Sign Up">
	</form>
@endsection
	
