@extends('layout.master')

@section('judul')
	{{$cast->nama}}
@endsection
	
@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Umur</label>
        <input type="text" class="form-control" name="umur" value="{{$cast->umur}}">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" value="{{$cast->bio}}"></textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
</div>
@endsection