<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function signup(){
        return view('halaman.register');
    }

    public function send(request $request){
        // dd($request->all());

        $fname = $request->firstname;
        $lname = $request->lastname;
        $gender = $request->gender;
        $nation = $request->nation;
        $lang = $request->lang;
        $bio = $request->bio;

        return view('halaman.welcome',compact('fname','lname','gender','nation','lang','bio'));
    }
}
